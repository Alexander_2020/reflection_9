﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.Json;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            const int countIteration = 1000_000;
            
            SerializeDeserializeMy(countIteration);
            SerializeDeserializeJson(countIteration);
        }

        private static void SerializeDeserializeMy(int countIteration)
        {
            Console.WriteLine("сериализация/десериализация рефлексия:");
            
            var serializedStrings = new List<string>();
            var serializer = new ReflectionSerializerDeserializer();
            var stopwatch = Stopwatch.StartNew();

            for (var i = 0; i < countIteration; i++)
            {
                serializedStrings.Add(serializer.Serialize(new F(1, 2, 3, 4, 5)
                {
                    I = 77, S = "str"
                }));    
            }
            
            stopwatch.Stop();
            Console.WriteLine($"сериализация {countIteration} шт: {stopwatch.Elapsed.TotalMilliseconds}");
            stopwatch.Reset();
            stopwatch.Start();

            foreach (var serializedString in serializedStrings)
            {
                var tmp = serializer.Deserialize<F>(serializedString);
            }
            
            stopwatch.Stop();
            Console.WriteLine($"дериализация {countIteration} шт: {stopwatch.Elapsed.TotalMilliseconds}");

        }

        private static void SerializeDeserializeJson(int countIteration)
        {
            Console.WriteLine("сериализация/десериализация json (System.Text.Json):");
            var serializedStrings = new List<string>();
            var stopwatch = Stopwatch.StartNew();

            for (var i = 0; i < countIteration; i++)
            {
                serializedStrings.Add(JsonSerializer.Serialize(new F(1, 2, 3, 4, 5)
                {
                    I = 77, S = "str"
                }));    
            }
            
            stopwatch.Stop();
            Console.WriteLine($"сериализация {countIteration} шт: {stopwatch.Elapsed.TotalMilliseconds}");
            stopwatch.Reset();
            stopwatch.Start();

            foreach (var serializedString in serializedStrings)
            {
                JsonSerializer.Deserialize<F>(serializedString);
            }
            
            stopwatch.Stop();
            Console.WriteLine($"дериализация {countIteration} шт: {stopwatch.Elapsed.TotalMilliseconds}");
        }
    }
}