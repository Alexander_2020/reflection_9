using System;
using System.Globalization;
using System.Reflection;
using System.Text;
using Microsoft.CSharp.RuntimeBinder;
using Binder = Microsoft.CSharp.RuntimeBinder.Binder;

namespace Reflection
{
    public class ReflectionSerializerDeserializer
    {
        private readonly BindingFlags _bindingFlags;
        private readonly string _separator;
        public ReflectionSerializerDeserializer()
        {
            _separator = ",";
            _bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
        }

        public string Serialize<T>(T obj)
        {
            var result = new StringBuilder();
            var typeT = typeof(T);
            
            foreach (var field in typeT.GetFields(_bindingFlags))
            {
                result.Append($"{field.Name}:{field.GetValue(obj)?.ToString()}{_separator}");
            }

            // foreach (var property in typeT.GetProperties(_bindingFlags))
            // {
            //     result.Append($"{property.Name}:{property.GetValue(obj)?.ToString()}");
            // }

            return result.ToString();
        }

        public T Deserialize<T>(string data) where T: new()
        {
            var typeObj = typeof(T);
            var obj = Activator.CreateInstance<T>();
            
            if (!string.IsNullOrEmpty(data))
            {
                foreach (var fields in data.Split(_separator, StringSplitOptions.RemoveEmptyEntries))
                {
                    var field = fields.Split(':', StringSplitOptions.None);
                    var fieldInfo = typeObj.GetField(field[0], _bindingFlags);

                    if (fieldInfo != null)
                    {
                        fieldInfo.SetValue(obj, Convert.ChangeType(field[1], fieldInfo.FieldType) );
                    }
                }
            }
            return  obj;
        }
    }
}